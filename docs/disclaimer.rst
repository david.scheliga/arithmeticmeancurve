**********
Disclaimer
**********
This documentation and the related python module `arithmeticmeancurve` are furnished as
they are, are for informational use only. The information and functionality within the
documentation and package are subject to change without notice and should not be
construed as a commitment by Dr. David Scheliga. Dr. David Scheliga assumes no
responsibility or liability for any errors or inaccuracies that may appear in the
informational content of the documentation and functional content of the python module
`arithmeticmeancurve`.