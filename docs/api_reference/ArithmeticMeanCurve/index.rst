***********************************
arithmeticcurve.ArithmeticMeanCurve
***********************************

.. autosummary::
   :toctree:

    arithmeticmeancurve.ArithmeticMeanCurve
    arithmeticmeancurve.ArithmeticMeanCurve.family_of_curves
    arithmeticmeancurve.ArithmeticMeanCurve.mean_curve
    arithmeticmeancurve.ArithmeticMeanCurve.scatter_curve
    arithmeticmeancurve.ArithmeticMeanCurve.std_circle
