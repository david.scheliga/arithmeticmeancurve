*****************************
arithmeticcurve API-reference
*****************************

.. autosummary::
   :toctree:

    arithmeticmeancurve.convert_set_to_family_of_curves
    arithmeticmeancurve.meld_set_of_curves_to_family
    arithmeticmeancurve.FrozenStdExtrapolation

.. toctree::

    ArithmeticMeanCurve/index
