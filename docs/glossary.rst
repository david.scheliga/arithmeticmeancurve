*******************************
Glossary of arithmeticmeancurve
*******************************


.. glossary::

    Set of curves
        A set of unique curves with separately x-values. Although it is a family of
        curves, their characteristic is, that the curves doesn't share a common
        x-values.

    Raw family of curves
        A family of curves which share common x-values, but contain only their original
        y-values.

    Family of curves
        A family of curves in which each curve share common x-values and contains
        interpolated values for added x-values.

    x-value
        The abscissa value of a curve.

    x-values
        Multiple sequence of x-value along the abscissa.

    y-value
        The ordinate value of a curve.

    y-values
        Multiple sequence of y-value along the ordinate.
